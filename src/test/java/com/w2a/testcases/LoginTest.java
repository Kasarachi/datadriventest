package com.w2a.testcases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.w2a.base.TestBase;



public class LoginTest extends TestBase {
	
	
		
	@Test
	public void LoginAsAdmin() throws InterruptedException {
		
		
		driver.findElement(By.xpath(OR.getProperty("UserName"))).sendKeys(OR.getProperty("Name")); 
		
		driver.findElement(By.xpath(OR.getProperty("Password"))).sendKeys(OR.getProperty("Pass"));
		
		log.debug("Inside Login Test");
		

		driver.findElement(By.xpath(OR.getProperty("IndigoLoginBtn"))).click();
		
		Thread.sleep(2000);
		Assert.assertTrue(isElementPresent(By.xpath(OR.getProperty("SearchPatient"))),"Couldn't find element");
		
		log.debug("Login Successfully executed");
		
	}

}
