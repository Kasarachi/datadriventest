package com.w2a.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.w2a.base.TestBase;

public class AppointmentPurpose extends TestBase {
	
	

	
	@Test(dataProvider="getData")
	public void addRoles(String purpose, String Description) {
		
		driver.findElement(By.xpath(OR.getProperty("Settings"))).click();
		
		driver.findElement(By.xpath(OR.getProperty("AppPurpose"))).click();
		
		driver.findElement(By.xpath(OR.getProperty("AddNewPurpose"))).click();
		
		driver.findElement(By.xpath(OR.getProperty("purpose"))).sendKeys(purpose);
		
		driver.findElement(By.xpath(OR.getProperty("Description"))).sendKeys(Description);
		
		driver.findElement(By.xpath(OR.getProperty("Save"))).click();
		
		
			
	}
	
	@DataProvider
	public Object[][] getData(){
		String sheetName = "AppointmentPurpose";
		
		int rows = excel.getRowCount(sheetName);
		int cols = excel.getColumnCount(sheetName);

		Object[][] data = new Object[rows - 1][cols];
		
		for (int rowNum = 2; rowNum <= rows; rowNum++) { // 2
			
			for (int colNum = 0; colNum < cols; colNum++) {

			
				data[rowNum - 2][colNum] = excel.getCellData(sheetName, colNum, rowNum);
			}

		}

		return data;

		
		
	}
	

}
